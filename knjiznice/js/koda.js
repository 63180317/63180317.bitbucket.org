/* global $ */
/* global btoa*/
/* global google*/


var baseUrl = 'https://rest.ehrscape.com/rest/v1';
//var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";



//document.getElementById("povezava").addEventListener("click", generirajPodatke());
	
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {

  var dropPacienti = document.getElementById("drop_pacienti");
  var listPacienti = document.getElementById("list_pacienti");
  var pacienti = document.createElement("select");
  
  pacienti.id = "EhrID";
  dropPacienti.appendChild(pacienti);
 
  var option_pacient = document.createElement("option");
	option_pacient.value = 0;
	option_pacient.text = "-- izberite pacienta --";
	pacienti.appendChild(option_pacient);
	
				
	var dropMeritve = document.getElementById("osnovne_meritve");
  listPacienti.innerHTML ="<b>Generirani podatki o pacientih:</b>";
  

// prvi pacient
  
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "France",
          lastNames: "Preseren",
          dateOfBirth: "1910-12-03",
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            
          		var compositionData = [{
                  "ctx/time": "2000-10-11T15:10Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 36.4,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 120,
                  "vital_signs/blood_pressure/any_event/diastolic": 80,
                  "vital_signs/height_length/any_event/body_height_length": 182,
                  "vital_signs/body_weight/any_event/body_weight": 87.3
                },{
                  "ctx/time": "2014-11-22T10:22Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 39.4,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 130,
                  "vital_signs/blood_pressure/any_event/diastolic": 90,
                  "vital_signs/height_length/any_event/body_height_length": 181,
                  "vital_signs/body_weight/any_event/body_weight": 80.8
                },{  
                  "ctx/time": "2015-01-15T22:17Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 40.2,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 90,
                  "vital_signs/blood_pressure/any_event/diastolic": 60,
                  "vital_signs/height_length/any_event/body_height_length": 181,
                  "vital_signs/body_weight/any_event/body_weight": 73.3
                },{
                  "ctx/time": "2015-02-01T11:35Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 41.1,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 80,
                  "vital_signs/blood_pressure/any_event/diastolic": 50,
                  "vital_signs/height_length/any_event/body_height_length": 181,
                  "vital_signs/body_weight/any_event/body_weight": 65.4
                }];
          		for(var st = 0; st < 4; st++){
          		  var queryParams = {
          		    ehrId: ehrId,
          		    templateId: "Vital Signs",
          		    format: "FLAT",
          		    committer: "Chuck Norris"
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(queryParams),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(compositionData[st]),
                  headers: {
                    "Authorization": getAuthorization()
                  },
						success: function () {
					//        $("#header").html("Store composition");
					//        $("#result").html(res.meta.href);
					      
						}
					});
				}                               
         
        var option_pacient = document.createElement("option");
				option_pacient.value = ehrId;
				option_pacient.text = "France Preseren";
				pacienti.appendChild(option_pacient);   
				
				listPacienti.innerHTML += "<br>France Preseren: " + ehrId;
				
				
            }
        });
        
    }

});

// drugi pacient

$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "Jaka",
          lastNames: "Racman",
          dateOfBirth: "2000-08-08",
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            
          		var compositionData = [{
                  "ctx/time": "2015-10-12T11:17Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 36.3,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 110,
                  "vital_signs/blood_pressure/any_event/diastolic": 80,
                  "vital_signs/height_length/any_event/body_height_length": 159,
                  "vital_signs/body_weight/any_event/body_weight": 55.7
                },{
                  "ctx/time": "2017-10-12T10:10Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 36.4,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 112,
                  "vital_signs/blood_pressure/any_event/diastolic": 83,
                  "vital_signs/height_length/any_event/body_height_length": 164,
                  "vital_signs/body_weight/any_event/body_weight": 62.3
                },{  
                  "ctx/time": "2018-02-12T20:10Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 36.7,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 119,
                  "vital_signs/blood_pressure/any_event/diastolic": 88,
                  "vital_signs/height_length/any_event/body_height_length": 167,
                  "vital_signs/body_weight/any_event/body_weight": 65.9
                },{
                  "ctx/time": "2019-01-11T14:24Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 36.4,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 117,
                  "vital_signs/blood_pressure/any_event/diastolic": 85,
                  "vital_signs/height_length/any_event/body_height_length": 173,
                  "vital_signs/body_weight/any_event/body_weight": 70.1
                }];
          		for(var st = 0; st < 4; st++){
          		  var queryParams = {
          		    ehrId: ehrId,
          		    templateId: 'Vital Signs',
          		    format: 'FLAT',
          		    committer: "Chuck Norris"
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(queryParams),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(compositionData[st]),
                  headers: {
                    "Authorization": getAuthorization()
                  },
						success: function () {
						   
						}
					});
				}                               
         
        var option_pacient = document.createElement("option");
				option_pacient.value = ehrId;
				option_pacient.text = "Jaka Racman";
				pacienti.appendChild(option_pacient);
							
				listPacienti.innerHTML += "<br>Jaka Racman: " + ehrId;	
            }
        });
        
    }

});

// tretji pacient

$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "Pika",
          lastNames: "Nogavicka",
          dateOfBirth: "2005-03-10",
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            
          		var compositionData = [{
                  "ctx/time": "2011-11-01T12:25Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 36.8,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 120,
                  "vital_signs/blood_pressure/any_event/diastolic": 80,
                  "vital_signs/height_length/any_event/body_height_length": 113,
                  "vital_signs/body_weight/any_event/body_weight": 27.8
                },{
                  "ctx/time": "2013-10-03T08:19Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 39.7,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 134,
                  "vital_signs/blood_pressure/any_event/diastolic": 95,
                  "vital_signs/height_length/any_event/body_height_length": 123,
                  "vital_signs/body_weight/any_event/body_weight": 32.4
                },{  
                  "ctx/time": "2015-08-19T19:18Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 40.2,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 121,
                  "vital_signs/blood_pressure/any_event/diastolic": 88,
                  "vital_signs/height_length/any_event/body_height_length": 129,
                  "vital_signs/body_weight/any_event/body_weight": 35.6
                },{
                  "ctx/time": "2017-11-12T22:32Z",
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": 39.7,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": 117,
                  "vital_signs/blood_pressure/any_event/diastolic": 89,
                  "vital_signs/height_length/any_event/body_height_length": 145,
                  "vital_signs/body_weight/any_event/body_weight": 47.8
                }];
          		for(var st = 0; st < 4; st++){
          		  var queryParams = {
          		    ehrId: ehrId,
          		    templateId: 'Vital Signs',
          		    format: 'FLAT',
          		    committer: "Chuck Norris"
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(queryParams),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(compositionData[st]),
                  headers: {
                    "Authorization": getAuthorization()
                  },
						success: function () {
						   
						}
					});
				}                               
        
        var option_pacient = document.createElement("option");
				option_pacient.value = ehrId;
				option_pacient.text = "Pika Nogavicka";
				pacienti.appendChild(option_pacient);
						
				listPacienti.innerHTML += "<br>Pika Nogavicka: " + ehrId;	
				
				var osnovne_meritve = document.getElementById("osnovne_meritve");
				osnovne_meritve.style.visibility="visible";
            }
        });
        
        
        pacienti.addEventListener("change",beriPodatke());
    }

});

    

}


function beriPodatke() {
  var ehrID = document.getElementById("EhrID").value;
	var osnovne_meritve = document.getElementById("osnovne_meritve").value;

  
	if ((ehrID.length > 1 ) && (osnovne_meritve.length > 1 )) {
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrID + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				var osnovni_podatki = document.getElementById("osnovni_podatki");
  				osnovni_podatki.innerHTML = "Ime in priimek pacienta: <b>"+party.firstNames + " " + party.lastNames + "</b><br>ID stevilka: "+ ehrID + "<br>Datum rojstva: "+ party.dateOfBirth;

         
	    	  $.ajax({
    				  url: baseUrl + "/view/" + ehrID + "/" + osnovne_meritve,
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {

/*   			    	
    			    	    var out_tab = document.createElement('table');
                    out_tab.style.width = '100%';
                    out_tab.setAttribute('border', '1');
                    var thead = document.createElement('thead');
                    var tr = document.createElement('tr');
                    var td = document.createElement('td');
                    td.appendChild(document.createTextNode('\u0020'));
                    tr.appendChild(td);
                    tr.appendChild(td);
                    thead.appendChild(tr);
                    out_tab.appendChild(thead);
                    out_tab.rows[0].cells[0].innerHTML = "Datum in cas";
*/                    
                    var out_tab = '<table width="100%" style="border:1px solid black"><tr style="border-bottom:1px solid black"><th>Datum in cas</th>';

    				        var tab_meritve = [];
    				        
    				        var tab_cas = [];
    				        var stevec = 1;
    				        var leto = "";
    				        var title= '';

                    switch(osnovne_meritve) {
                      
                      case "height":

//                        out_tab.rows[0].cells[1].innerHTML = "Visina";
                        out_tab = out_tab + '<th>Visina</th></tr>';
                        title = 'Višina';
                        
                        for (var st in res) {

                           out_tab = out_tab + '<tr><td>'+res[st].time+'</td>';
                           out_tab = out_tab + '<td>'+res[st].height + ' ' + res[st].unit+'</td></tr>';
                           
                           
                            leto = res[st].time;
                            leto = leto.substring(0, 10);
                            
                            var sub_tab_meritve = [];
                            sub_tab_meritve[0]=leto;
                            sub_tab_meritve[1]=res[st].height;
                            tab_meritve.push(sub_tab_meritve);
/*                          
                          var tr = document.createElement('tr');
                          var td = document.createElement('td');
                          td.appendChild(document.createTextNode('\u0020'));
                          tr.appendChild(td);
                          tr.appendChild(td);
                          thead.appendChild(tr);
                          out_tab.rows[st].cells[0].innerHTML = res[st].time;
                          out_tab.rows[st].cells[1].innerHTML = res[st].height +
                            " " + res[st].unit;
        				        */
        				        }
        				        
        				        
        				        out_tab = out_tab + '</table>';
        				        document.getElementById("meritve").innerHTML = out_tab;
        				        console.log(tab_meritve);
        				        
                        break;
                        
                        
                      case "weight":

                        out_tab = out_tab + '<th>Teza</th></tr>';
                        title = 'Teža';
                        
                        for (var st in res) {

                           out_tab = out_tab + '<tr><td>'+res[st].time+'</td>';
                           out_tab = out_tab + '<td>'+res[st].weight + ' ' + res[st].unit+'</td></tr>';
                           
                           
                            leto = res[st].time;
                            leto = leto.substring(0, 10);
                            
                            var sub_tab_meritve = [];
                            sub_tab_meritve[0]=leto;
                            sub_tab_meritve[1]=res[st].weight;
                            tab_meritve.push(sub_tab_meritve);
        				        }
        				        
        				        
        				        out_tab = out_tab + '</table>';
        				        document.getElementById("meritve").innerHTML = out_tab;
                        break;
                        
                      case "body_temperature":

                        out_tab = out_tab + '<th>Temperatura</th></tr>';
                        title = 'Telesna temperatura';
                        
                        for (var st in res) {

                           out_tab = out_tab + '<tr><td>'+res[st].time+'</td>';
                           out_tab = out_tab + '<td>'+res[st].temperature + ' ' + res[st].unit+'</td></tr>';
                           console.log(res);
                           
                            leto = res[st].time;
                            leto = leto.substring(0, 10);
                            
                            var sub_tab_meritve = [];
                            sub_tab_meritve[0]=leto;
                            sub_tab_meritve[1]=res[st].temperature;
                            tab_meritve.push(sub_tab_meritve);
        				        }
        				        
        				        
        				        out_tab = out_tab + '</table>';
        				        document.getElementById("meritve").innerHTML = out_tab;
                        break;
                        
                      case "blood_pressure":

                        out_tab = out_tab + '<th>Sistolični</th>';
                        out_tab = out_tab + '<th>Diastolični</th></tr>';
                        title = 'Krvni pritisk';
                        
                        for (var st in res) {

                           out_tab = out_tab + '<tr><td>'+res[st].time+'</td>';
                           out_tab = out_tab + '<td>'+res[st].systolic + ' ' + res[st].unit+'</td>';
                           out_tab = out_tab + '<td>'+res[st].diastolic + ' ' + res[st].unit+'</td></tr>';
                           console.log(res);
                           
                            leto = res[st].time;
                            leto = leto.substring(0, 10);
                            
                            var sub_tab_meritve = [];
                            sub_tab_meritve[0]=leto;
                            sub_tab_meritve[1]=res[st].systolic;
                            sub_tab_meritve[2]=res[st].diastolic;
                            tab_meritve.push(sub_tab_meritve);
        				        }
        				        
        				        
        				        out_tab = out_tab + '</table>';
        				        document.getElementById("meritve").innerHTML = out_tab;
                        break;
                        
                      default:
                        document.getElementById("meritve").innerHTML = "";
                        break;
                    }
                    
                    
                  if (osnovne_meritve == "blood_pressure" ) {
                    
                    google.charts.load('current', {packages: ['corechart','line']}); 
                    google.charts.setOnLoadCallback(drawChart_pressure);
    				        
                    function drawChart_pressure() {
                      // Define the chart to be drawn.
                      var data = new google.visualization.DataTable();
                      data.addColumn('string', 'Cas');
                      data.addColumn('number', 'Sistolični');
                      data.addColumn('number', 'Diastolični');
                      data.addRows(tab_meritve.reverse());
                         
                      // Set chart options
                      var options = {'title' : '',
                         hAxis: {
                            title: 'Leta'
                         },
                         vAxis: {
                            title: title
                         },   
                         'width':550,
                         'height':400	  
                      };
  
                      var chart = new google.visualization.LineChart(document.getElementById('chart'));
                      chart.draw(data, options);
                   } 
                    
                  } else
                  {
                    google.charts.load('current', {packages: ['corechart','line']}); 
                    google.charts.setOnLoadCallback(drawChart);
    				        
                    function drawChart() {
                      // Define the chart to be drawn.
                      var data = new google.visualization.DataTable();
                      data.addColumn('string', 'Cas');
                      data.addColumn('number', 'Sprememba ');
                      data.addRows(tab_meritve.reverse());
                         
                      // Set chart options
                      var options = {'title' : '',
                         hAxis: {
                            title: 'Leta'
                         },
                         vAxis: {
                            title: title
                         },   
                         'width':550,
                         'height':400	  
                      };
  
                      var chart = new google.visualization.LineChart(document.getElementById('chart'));
                      chart.draw(data, options);
                   } 
                  }
    			    }
  					});
  				}
		});
	}
	else
	{
//	  document.getElementById("osnovni_podatki").innerHTML = "";
	  document.getElementById("meritve").innerHTML = "";
	  document.getElementById("chart").innerHTML = "";
	}
  
}  

function fDuckDuckGo(){
  
  $.ajax({
    url:"https://duckduckgo-duckduckgo-zero-click-info.p.rapidapi.com/?no_redirect=1&no_html=1&callback=process_duckduckgo&skip_disambig=1&q=DuckDuckGo&format=json",
    type: 'GET',
    headers: {
      "X-RapidAPI-Host": "duckduckgo-duckduckgo-zero-click-info.p.rapidapi.com",
      "X-RapidAPI-Key": "SIGN-UP-FOR-KEY",
      "accept": "application/json",
      "Authorization": getAuthorization()
    },
    success: function (result) {
      console.log(result.status, result.headers, result.body);
    }

  });
}


document.addEventListener('change',function(e){
/*  
    document.getElementById("osnovni_podatki").innerHTML = "";
	  document.getElementById("meritve").innerHTML = "";
	  document.getElementById("chart").innerHTML = "";
	  */
	  beriPodatke()
 });